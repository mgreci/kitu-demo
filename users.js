const convertUsers = users => {
  return users.map(user => {
    return {
      gender: user.gender,
      firstname: user.name.first,
      city: user.location.city,
      email: user.email,
      cell: user.cell
    };
  });
};

const createUsers = (storage, users) => {
  users.forEach(user => storage.push(user));
};

const findUsers = (storage, firstname) => {
  return storage.filter(user => {
    return user.firstname.toLowerCase() === firstname.toLowerCase()
  });
};

module.exports = {
  convertUsers: convertUsers,
  findUsers: findUsers,
  createUsers: createUsers
};
