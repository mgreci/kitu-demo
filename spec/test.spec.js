let convertUsers = require('../users.js').convertUsers;
let createUsers = require('../users.js').createUsers;
let findUsers = require('../users.js').findUsers;

describe("Users", () => {
  it("should convert an array of users", () => {
    const apiData = [{
      gender: "female",
      name: {
        first: "Angie"
      },
      location: {
        city: "San Diego"
      },
      email: "angie.test@example.com",
      cell: "123-456-1234"
    }];

    const result = convertUsers(apiData);

    const expected = [{
      gender: 'female',
      firstname: 'Angie',
      city: 'San Diego',
      email: 'angie.test@example.com',
      cell: '123-456-1234'
    }];

    expect(result).toEqual(expected);
  });

  it("should add array of users to storage", () => {
    let users = [{
      gender: 'male',
      firstname: 'John',
      city: 'Riverside',
      email: 'john.person@example.com',
      cell: '321-321-4321'
    }];

    let fakeInMemoryStorage = [];

    createUsers(fakeInMemoryStorage, users);

    const expected = [{
      gender: 'male',
      firstname: 'John',
      city: 'Riverside',
      email: 'john.person@example.com',
      cell: '321-321-4321'
    }];

    expect(fakeInMemoryStorage).toEqual(expected);
  });

  describe("User search", () => {
    it("should find user by first name", () => {
      const firstNameToSearch = "Matthew";

      const fakeInMemoryStorage = [{
        gender: 'male',
        firstname: 'John',
        city: 'Riverside',
        email: 'john.person@example.com',
        cell: '321-321-4321'
      }, {
        gender: 'female',
        firstname: 'Angie',
        city: 'San Diego',
        email: 'angie.test@example.com',
        cell: '123-456-1234'
      }, {
        gender: 'female',
        firstname: 'alma',
        city: 'Irvine',
        email: 'alma.last@example.com',
        cell: '123-456-7890'
      }, {
        gender: 'male',
        firstname: 'Matthew',
        city: 'San Diego',
        email: 'matthew@greci.tech',
        cell: '098-098-0987'
      }];

      const result = findUsers(fakeInMemoryStorage, firstNameToSearch);

      const expected = [{
        gender: 'male',
        firstname: 'Matthew',
        city: 'San Diego',
        email: 'matthew@greci.tech',
        cell: '098-098-0987'
      }];

      expect(result).toEqual(expected);
    });

    it("should find user, case insensitive", () => {
      const firstNameToSearch = "MatTHew";

      const fakeInMemoryStorage = [{
        gender: 'male',
        firstname: 'John',
        city: 'Riverside',
        email: 'john.person@example.com',
        cell: '321-321-4321'
      }, {
        gender: 'female',
        firstname: 'Angie',
        city: 'San Diego',
        email: 'angie.test@example.com',
        cell: '123-456-1234'
      }, {
        gender: 'female',
        firstname: 'alma',
        city: 'Irvine',
        email: 'alma.last@example.com',
        cell: '123-456-7890'
      }, {
        gender: 'male',
        firstname: 'matthew',
        city: 'San Diego',
        email: 'matthew@greci.tech',
        cell: '098-098-0987'
      }];

      const result = findUsers(fakeInMemoryStorage, firstNameToSearch);

      const expected = [{
        gender: 'male',
        firstname: 'matthew',
        city: 'San Diego',
        email: 'matthew@greci.tech',
        cell: '098-098-0987'
      }];

      expect(result).toEqual(expected);
    });
  });

});
