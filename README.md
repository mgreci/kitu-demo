# Kitu Coding Exercise
#### Created by Matthew Greci matthew@greci.tech
The requirements for this project as available in `KituCodingExercise.txt`.

## Assumptions
1. I assume the structure of a JSON provided to the POST /users is the following:
```
{
  "gender": "male",
  "firstname": "Matthew",
  "city": "San Diego",
  "email": "test@test.com",
  "cell": "123-123-1234"
}
```

2. I assume when searching users by firstname, there can be multiple users with a matching firstname so I have opted to return an array of matching user objects.

## Setup

You will need Node/npm installed on your machine. Run the following from this directory to install prerequisites for this project.
```shell
npm install
```

## Run
Start the application (runs on port 4000):
```shell
npm start
```

## Testing

In order to setup the testing environment, run the following to install prerequisites (at the global npm level).
```shell
npm install --global jasmine newman nyc
```

Run unit tests (jasmine and nyc). coverage report available in `coverage/index.html`).
```shell
npm run test-unit
```

Run API tests (ensure the app is running first) with:
```shell
npm run test-api
```

## Docker
An alternative for setup, testing, and running this project is to use to provided `Dockerfile`. Ensure Docker is installed. Then run the following (subsitute DESIRED_PORT) for a quick setup and run.
```shell
docker image build -t kitu_demo_greci .
docker run -d -p DESIRED_PORT:4000 kitu_demo_greci
```

Docker eliminates the need for the end system to have Node/npm installed as well as global npm packages (including worried about which version(s) of everything) and increases the portability of the application. The main downside is most systems do not have Docker already installed, although that is a small amount of setup time.
