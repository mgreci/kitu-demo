const myModule = require('./users.js');
const axios = require('axios');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const PORT = 4000;

app.use(bodyParser.json());

let storage = [];

app.get('/users', (req, res) => {
  const URL = "https://randomuser.me/api?inc=gender,name,location,email,cell&results=10";
  axios.get(URL)
  .then(response => response.data.results)
  .then(users => {
      myModule.createUsers(storage, myModule.convertUsers(users));
      res.json(storage);
  })
  .catch(error => res.json({error: error}));
});

app.post('/users', (req, res) => {
  myModule.createUsers(storage, [req.body]);
  res.status(201).json({ message: 'User successfully created!' });
});

app.get('/users/firstname/:firstname', (req, res) => {
  let results = myModule.findUsers(storage, req.params.firstname);
  if (results.length > 0) {
    res.json(results);
  } else {
    res.status(404).json({message: 'User not found! '});
  }
});

app.listen(PORT, () => console.log(`listening on PORT: ${PORT}`));
