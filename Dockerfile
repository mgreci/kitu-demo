FROM node:slim
RUN npm install --global nyc jasmine newman
WORKDIR /app
ADD package*.json ./
RUN npm install
ADD . ./
CMD ["npm", "start"]
